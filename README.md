# atomic_compare_exchange VS direct assignment

## to run direct_vs_atomic.c

```
gcc direct_vs_atomic.c -lm
taskset -c 5 ./a.out
```

## to run hashmap_vs_array.c

```
//gcc hashmap_vs_array.c src/hashmap.c -I include/emumba/ -lm
//taskset -c 5 ./a.out
```
