//gcc direct_vs_atomic.c -lm
//taskset -c 5 ./a.out

//#include <iostream>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
// using namespace std;
int nums = 1000000;
double list[1000000];
double std;
double mean;
double sum1;

/*_Atomic bool futexp, one, pid, x, y;
_Atomic unsigned long l_futexp, l_one, l_pid, l_x, l_y;
_Atomic short s_futexp, s_one, s_pid, s_x, s_y;
_Atomic char u8_futexp, u8_one, u8_pid, u8_x, u8_y;
*/
bool futexp, one, pid, x, y;
unsigned long l_futexp, l_one, l_pid, l_x, l_y;
short s_futexp, s_one, s_pid, s_x, s_y;
char u8_futexp, u8_one, u8_pid, u8_x, u8_y;

enum lock_status
{
  LOCKED,
  UNLOCKED
};
enum lock_status e_futexp, e_one, e_pid, e_x, e_y;
struct timespec start_time, end_time;
double get_elapsed_ns()
{
  return ((1000000000.0 * (double)(end_time.tv_sec - start_time.tv_sec)) + (double)(end_time.tv_nsec - start_time.tv_nsec));
}

double get_std()
{
  sum1 = 0;
  for (int i = 0; i < nums; i++)
  {
    sum1 = sum1 + (list[i] - mean)*(list[i] - mean);
  }

  return sqrt(sum1/nums);
}

inline double get_elapsed_us()
{
  return get_elapsed_ns() / 1000.0;
}
int main()
{
  one = 1;
  futexp = 0;
  pid = 0;
  double sum = 0;
  for (int i = 0; i < nums; i++)
  {
    futexp = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_compare_exchange_strong(&futexp, &one, pid);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("bool with atomic_compare_exchange=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  x = 0;
  y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    x = i;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("bool with direct assignment=%fns\t\t standard dev=%f\n", mean, std);

  sum = 0;
  x = 0;
  y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_exchange(&x, i);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("bool with atomic_exchange=%fns\t\t standard dev=%f\n\n", mean, std);

  e_one = 1;
  e_futexp = 0;
  e_pid = 0;
  sum = 0;
  for (int i = 0; i < nums; i++)
  {
    e_futexp = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_compare_exchange_strong(&e_futexp, &e_one, e_pid);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("enum with atomic_compare_exchange=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  e_x = 0;
  e_y = 1;
  for (int i = 0; i < nums; i++)
  {
    e_y = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    e_x = e_y;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("enum with drect assignment=%fns\t\t standard dev=%f\n", mean, std);

  sum = 0;
  e_x = 0;
  e_y = 1;
  for (int i = 0; i < nums; i++)
  {
    e_y = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_exchange(&e_x, e_y);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("enum with atomic_exchange=%fns\t\t standard dev=%f\n\n", mean, std);

  l_one = 123456789;
  l_futexp = 0;
  l_pid = 234567891;
  sum = 0;
  for (int i = 0; i < nums; i++)
  {
    l_futexp = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_compare_exchange_strong(&l_futexp, &l_one, l_pid);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("long with atomic_compare_exchange=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  l_x = 0;
  l_y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    l_x = i;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("long with direct assignment=%fns\t\t standard dev=%f\n", mean, std);

  sum = 0;
  l_x = 0;
  l_y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_exchange(&l_x, i);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("long with atomic_exchange=%fns\t\t standard dev=%f\n\n", mean, std);

  s_one = 1234;
  s_futexp = 0;
  s_pid = 2345;
  sum = 0;
  for (int i = 0; i < nums; i++)
  {
    s_futexp = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_compare_exchange_strong(&s_futexp, &s_one, s_pid);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("short with atomic_compare_exchange=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  s_x = 0;
  s_y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    s_x = i;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("short with direct assignment=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  s_x = 0;
  s_y = 1;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_exchange(&s_x, i);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("short with atomic_exchange=%fns\t\t standard dev=%f\n\n", mean, std);

  u8_one = 123;
  u8_futexp = 0;
  u8_pid = 234;
  sum = 0;
  for (int i = 0; i < nums; i++)
  {
    u8_futexp = i%2;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_compare_exchange_strong(&u8_futexp, &u8_one, u8_pid);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("u_int8 with atomic_compare_exchange=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  u8_x = 0;
  u8_y = 1;
  for (int i = 0; i < nums; i++)
  {
    u8_y = i%8;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    u8_x = u8_y;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("u_int8 with direct assignment=%fns\t standard dev=%f\n", mean, std);

  sum = 0;
  u8_x = 0;
  u8_y = 1;
  for (int i = 0; i < nums; i++)
  {
    u8_y = i%8;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    atomic_exchange(&u8_x, u8_y);
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
  printf("u_int8 with atomic_exchange=%fns\t\t standard dev=%f\n", mean, std);
}