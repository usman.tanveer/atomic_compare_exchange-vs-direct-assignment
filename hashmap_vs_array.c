//gcc hashmap_vs_array.c src/hashmap.c -I include/emumba/ -lm
//taskset -c 5 ./a.out

//#include <iostream>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <hashmap.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
//#include <rte_malloc.h>
//using namespace std;

struct hashmap *map;
enum fd_types {NO_FD, FSTACK, KERNEL, MIXED};
enum block_status {NONBLOCK, BLOCK, SND_TIMEOUT, RCV_TIMEOUT,SND_AND_RCV_TIMEOUT};
struct timespec start_time, end_time;
int sum1, mean, std;
const int nums = 10000;
double list[10000];

double get_elapsed_ns()
{
  return ((1000000000.0 * (double)(end_time.tv_sec - start_time.tv_sec)) + (double)(end_time.tv_nsec - start_time.tv_nsec));
}

double get_std()
{
  sum1 = 0;
  for (int i = 0; i < nums; i++)
  {
    sum1 = sum1 + (list[i] - mean)*(list[i] - mean);
  }

  return sqrt(sum1/nums);
}

inline double get_elapsed_us()
{
  return get_elapsed_ns() / 1000.0;
}

struct fd_details{
    int dup_count;
    int fork_count;
    enum fd_types fd_type; 
    int kernel_skip_counter; 
    enum block_status block_status; 
    struct timeval recv_timeout;
    struct timeval snd_timeout;
};

struct fd_info{
    int kern_fd;
    int fstack_fd;
    struct fd_details *detail_ptr;
};

unsigned int hash(unsigned int x) {
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

uint64_t user_hash(const void *item, uint64_t seed0, uint64_t seed1) {
    const struct fd_info *fd = item;
    return hash(fd->kern_fd);
}


int user_compare(const void *a, const void *b, void *udata) {
    const struct fd_info *ua = a;
    const struct fd_info *ub = b;

    if (ua->kern_fd == ub->kern_fd)
        return 0;

    return 1;
}




int main()
{
map = hashmap_new(sizeof(struct fd_info), 10000, 0, 0, user_hash, user_compare, NULL);
for (int i =1; i<nums ; i++)
{
    struct fd_details *fd_detail_ptr = malloc (sizeof (struct fd_details));
    fd_detail_ptr->kernel_skip_counter=0;
    fd_detail_ptr->fd_type=KERNEL;
    fd_detail_ptr->recv_timeout.tv_sec=0;
    fd_detail_ptr->snd_timeout.tv_usec=0;
    fd_detail_ptr->block_status=BLOCK;
    fd_detail_ptr->dup_count=1;
    fd_detail_ptr->fork_count=0;
    hashmap_set(map, &(struct fd_info ){ .kern_fd=i,
                                        .fstack_fd=-1,
                                        .detail_ptr=fd_detail_ptr});
}

struct fd_info fd_array[10000];

for (int i =1; i<nums ; i++)
{
    struct fd_details *fd_detail_ptr = malloc (sizeof (struct fd_details));
    fd_detail_ptr->kernel_skip_counter=0;
    fd_detail_ptr->fd_type=KERNEL;
    fd_detail_ptr->recv_timeout.tv_sec=0;
    fd_detail_ptr->snd_timeout.tv_usec=0;
    fd_detail_ptr->block_status=BLOCK;
    fd_detail_ptr->dup_count=1;
    fd_detail_ptr->fork_count=0;

    fd_array[i].detail_ptr = fd_detail_ptr;
    fd_array[i].kern_fd = i;
    fd_array[i].fstack_fd = -1;
}

double sum = 0;
struct fd_info *fd_element;
  for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    fd_element = hashmap_get(map, &(struct fd_info){.kern_fd=i});
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
printf("mean time for hashmap=%d\tstd=%d\n", mean, std);


sum = 0;
struct fd_info * fd_info_element;
for (int i = 0; i < nums; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    fd_array[i];
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    list[i] = get_elapsed_ns();
    sum = sum + list[i];
  }
  mean = sum / nums;
  std = get_std();
printf("mean time for array=%d\t\tstd=%d\n", mean, std); 

/////////////////////////////////////////////////////////////

//int *mem;
//mem = rte_malloc(NULL, sizeof(int), 0);

}